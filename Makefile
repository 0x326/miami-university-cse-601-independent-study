LATEX_SOURCES := $(wildcard *.tex) $(wildcard *.bib)

build: Document.pdf

%.pdf: %.tex $(LATEX_SOURCES) $(LATEX_RESOURCES) $(REMOTE_RESOURCES)
	pdflatex --shell-escape $<
